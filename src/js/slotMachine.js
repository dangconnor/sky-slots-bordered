var SlotMachine = function() {

    // CONST
    // 
    this.SKY_CLASS = "slot-machine__reel-item--sky";

    //The score
    //
    this.score = 0;

    //The best score done
    //
    this.bestScore = 0;

    //The remaining spins
    //
    this.remainingSpins = 3;

    /**
     * Init the game
     */
    this.init = function() {
        this.initResize();
        this.initSlots();
    };

    /** 
     * Init window resize
     */
    this.initResize = function() {
        var self = this;

        $(window).resize(function() {   
            $(".slot-machine__container").remove();  
            $(".slot-machine__reel_container").remove(); 
            $(".slot-cover").remove(); 

            var template = $(".slot-machine__reel--template").html();
            $(template).appendTo(".slot-machine-content");
            self.initSlots();
            
        });

    }

    /**
     * Init the slots
     */
    this.initSlots = function() {
        var self = this;

        $("#play").unbind('click');
        $(".slot-machine .slot-machine__reel").jSlots({
            number : 3,
            winnerNumber : 99,
            spinner : "#play",
            easing : "easeOutSine",
            time : 3000,
            loops : 6,
            onStart : function() {
                self.start();
            },
            onEnd : function(finalNumbers) {
                self.setScore(finalNumbers);
            }
        });

        if(this.remainingSpins === 0) {
            $("#play").unbind('click');
        }

        var slotCount = 1;

        $(".slot-machine-content .slot-machine__reel").each(function(){
            $(this).before("<div class='slot-cover slot-cover-"+slotCount+"'></div>");
            slotCount++;
        });

        $(".slot-machine-content .slot-machine__reel").wrap( "<div class='slot-machine__reel_container'><div class='slot-machine__reel_container_inner'></div></div>" );
    };

    /**
     * Reset data
     *     - remove winner class
     *     - reset score
     */
    this.start = function() {
        $(".slot-machine .slot-machine__reel").removeClass("winner");
        this.remainingSpins--;
        $(".scoreboard__item--attempts").text(this.remainingSpins);
    };

    /**
     * Set the score
     */
    this.setScore = function(finalNumbers) {
        var hasWin = true;

        this.score = 0;

        // For each slot, check if it's a winner slot, then increment the score
        // if one of the slot is not a winner slot, the user doesn't win
        //
        for(var i = 0; i < finalNumbers.length; i++) {
            var $slot = $($(".slot-machine__reel")[i]),
                $item = $($slot.find("li")[finalNumbers[i] - 1]);

            if($item.hasClass(this.SKY_CLASS)) {
                this.score ++;
            }
            else {
                hasWin = false;
            }
        }

        //Update display
        //
        if(hasWin) {
            $(".slot-machine .slot-machine__reel").addClass("winner");
        }

        if(this.remainingSpins === 0) {
            $("#play").addClass("played").unbind('click');
        }

        $(".scoreboard__item--score").text(this.score);

        if(this.score > this.bestScore) {
            this.bestScore = this.score;
            $(".scoreboard__item--best").text(this.bestScore);
        }
    };
}

$("document").ready(function() {
    new SlotMachine().init();
});