module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                files: {
                    'src/css/main.css': 'src/sass/main.scss',
                }
            }
        },
        watch: {
            css: {
                files: ['src/sass/*.scss'],
                tasks: ['sass', 'autoprefixer']
            }
        },
        autoprefixer: {
            dist: {
                files: {
                    'src/css/main.css': 'src/css/main.css',
                }
            }
        },
        connect: {
            server: {
                options: {
                    port: 8000,
                    base: 'src'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('default', ['sass', 'autoprefixer', 'connect', 'watch']);
};